package uz.uzbeklion.niceweathernew2.retrofit;




import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import uz.uzbeklion.niceweathernew2.models.Example;

/**
 * Created by Sherzod on 02.02.2018.
 */

public interface ApiInterface {
    @GET("http://api.openweathermap.org/data/2.5/forecast?")
    Call<Example> getData(@Query("lat") double lat, @Query("lon") double lon,@Query("units") String units,@Query("appid") String api );

}
