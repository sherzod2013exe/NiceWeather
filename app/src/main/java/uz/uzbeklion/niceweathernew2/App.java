package uz.uzbeklion.niceweathernew2;

import android.app.Activity;
import android.app.Application;
import com.squareup.picasso.Picasso;
import uz.uzbeklion.niceweathernew2.retrofit.ApiInterface;
import uz.uzbeklion.niceweathernew2.dagger.ContextModule;
import uz.uzbeklion.niceweathernew2.dagger.DaggerWeatherComponent;
import uz.uzbeklion.niceweathernew2.dagger.WeatherComponent;

public class App extends Application {

    public static App get(Activity activity){
        return ((App) activity.getApplication());
    }
    private WeatherComponent component;

    private Picasso picasso;

    private ApiInterface anInterface;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerWeatherComponent.builder()
                .contextModule(new ContextModule(this))
                .build();
        picasso = component.getPicasso();
        anInterface = component.getApiInterface();

    }

    public WeatherComponent getComponent() {
        return component;
    }

    public Picasso getPicasso() {
        return picasso;
    }

    public ApiInterface getAnInterface() {
        return anInterface;
    }
}
