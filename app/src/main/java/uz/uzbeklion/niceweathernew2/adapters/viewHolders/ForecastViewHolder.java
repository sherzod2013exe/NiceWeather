package uz.uzbeklion.niceweathernew2.adapters.viewHolders;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import uz.uzbeklion.niceweathernew2.R;
import uz.uzbeklion.niceweathernew2.models.List;

/**
 * Created by Sherzod on 26.03.2018.
 */

public class ForecastViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.humidity)
    TextView humidity;
    @BindView(R.id.morning_weather)
    TextView temp_min;
    @BindView(R.id.night_weather)
    TextView temp_max;
    @BindView(R.id.main_weather)
    TextView main   ;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.day)
    TextView dt_txt;
    @BindView(R.id.day2)
    TextView dt_txt2;
    @BindView(R.id.weater_image_id)
    ImageView  imageView;
    @BindView(R.id.today_list_container)
    View view;

    public ForecastViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }

    public void bindView(final List example,Picasso picasso) {
        humidity.setText(example.getMain().getHumidity().toString());
        temp_min.setText(example.getMain().getTempMin().toString());
        temp_max.setText(example.getMain().getTempMax().toString());
        main.setText(example.getWeather().get(0).getMain().toString());
        description.setText(example.getWeather().get(0).getDescription().toString());
        String[] s= example.getDtTxt().split(" ");
        Uri uri =Uri.parse("http://openweathermap.org/img/w/"+example.getWeather().get(0).getIcon()+".png");
        picasso.get()
                .load(uri)
                .placeholder(R.drawable.humidity)
                .into(imageView);
        dt_txt.setText(s[0]);
        dt_txt2.setText(s[1]);

    }
}
