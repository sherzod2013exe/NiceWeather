package uz.uzbeklion.niceweathernew2.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.uzbeklion.niceweathernew2.BuildConfig;
import uz.uzbeklion.niceweathernew2.R;
import uz.uzbeklion.niceweathernew2.adapters.viewHolders.ForecastViewHolder;
import uz.uzbeklion.niceweathernew2.models.Example;
import uz.uzbeklion.niceweathernew2.models.List;
import uz.uzbeklion.niceweathernew2.retrofit.ApiInterface;

/**
 * Created by Sherzod on 26.03.2018.
 */

public class WeatherAdapter extends BaseAdapter<List> {
            private Picasso picasso;

            @Inject
            public WeatherAdapter(Picasso picasso) {
                this.picasso = picasso;

            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return new ForecastViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item,parent,false));
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                ((ForecastViewHolder) holder).bindView(data.get(position),picasso);
            }

            @Override
            public int getItemCount() {
                return data.size();
            }
}
