package uz.uzbeklion.niceweathernew2.adapters;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 26-Sep-17.
 */

public abstract class BaseAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected List<T> data;

    public void addItem(T item) {
        if(this.data == null) this.data = new ArrayList<T>();
        this.data.add(item);
        notifyItemInserted(this.data.size()-1);
    }

    public void addAfter(List<T> data) {
        if(this.data == null) this.data = new ArrayList<>();
        int beginIndex = this.data.size();
        this.data.addAll(data);
        notifyItemRangeInserted(beginIndex, data.size());
    }

    public void addBefore(List<T> data) {
        if(this.data == null) this.data = new ArrayList<>();
        data.addAll(this.data);
        this.data = data;
        notifyDataSetChanged();
    }

    public void setData(List<T> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void changeItem(T item, int position) {
        if(position < 0 || position >= getItemCount()) return;
        data.set(position, item);
        notifyItemChanged(position);
    }

    public void removeItem(int position) {
        if(position < 0 || position >= getItemCount()) return;
        this.data.remove(position);
        notifyItemRemoved(position);
    }

    public List<T> getData() {
        return data;
    }


}
