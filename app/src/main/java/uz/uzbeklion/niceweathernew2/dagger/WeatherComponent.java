package uz.uzbeklion.niceweathernew2.dagger;

import com.squareup.picasso.Picasso;

import dagger.Component;
import uz.uzbeklion.niceweathernew2.retrofit.ApiInterface;


@WeatherAplicationScope
@Component(modules = {WeatherServiceModule.class,PicassoModule.class})
public interface WeatherComponent {
    Picasso getPicasso();

    ApiInterface getApiInterface();
}
