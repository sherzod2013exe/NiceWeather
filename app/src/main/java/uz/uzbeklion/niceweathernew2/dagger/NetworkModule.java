package uz.uzbeklion.niceweathernew2.dagger;


import android.content.Context;

import java.io.File;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

@Module(includes = ContextModule.class)
public class NetworkModule {

   @Provides
   @WeatherAplicationScope
   public HttpLoggingInterceptor loggingInterceptor(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Timber.i(message);
            }
        });
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return interceptor;
    }

    @Provides
    @WeatherAplicationScope
    public Cache cache(File file){
       return new Cache(file,10*1024*1024);//10mb
    }

    @Provides
    @WeatherAplicationScope
    public File cacheFile(Context context){
       return new File(context.getCacheDir(),"okhttp_cache");
    }

    @Provides
    @WeatherAplicationScope
    public OkHttpClient okHttpClient(HttpLoggingInterceptor interceptor, Cache cache){
       return  new OkHttpClient.Builder()
               .addInterceptor(interceptor)
               .cache(cache)
               .build();
    }
}
