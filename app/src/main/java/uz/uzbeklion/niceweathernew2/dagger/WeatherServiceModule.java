package uz.uzbeklion.niceweathernew2.dagger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uz.uzbeklion.niceweathernew2.retrofit.ApiInterface;

@Module(includes = NetworkModule.class)
public class WeatherServiceModule {

    @Provides
    @WeatherAplicationScope
    public Retrofit getRetrofit(OkHttpClient okHttpClient,Gson gson){
        return new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/forecast/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }
    @Provides
    @WeatherAplicationScope
    public ApiInterface apiInterface(Retrofit retrofit){
        return retrofit.create(ApiInterface.class);
    }

    @Provides
    @WeatherAplicationScope
    public Gson gson(){
        GsonBuilder gsonBuilder  =new GsonBuilder();
        return gsonBuilder.create();
    }

}
