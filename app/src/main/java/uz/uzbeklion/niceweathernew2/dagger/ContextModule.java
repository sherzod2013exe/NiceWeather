package uz.uzbeklion.niceweathernew2.dagger;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {
    private final Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    @WeatherAplicationScope
    public Context context(){
        return context;
    }
}
