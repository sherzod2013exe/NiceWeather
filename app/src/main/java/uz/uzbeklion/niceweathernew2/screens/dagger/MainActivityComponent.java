package uz.uzbeklion.niceweathernew2.screens.dagger;


import dagger.Component;
import uz.uzbeklion.niceweathernew2.dagger.WeatherComponent;
import uz.uzbeklion.niceweathernew2.screens.MainActivity;

@Component(dependencies = WeatherComponent.class)
@MainActivityScope
public interface MainActivityComponent {

    void injectMainActivity(MainActivity mainActivity);

}
