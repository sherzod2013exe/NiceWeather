package uz.uzbeklion.niceweathernew2.screens.dagger;


import dagger.Module;
import uz.uzbeklion.niceweathernew2.screens.MainActivity;

@Module
public class MainActivityModule {
    private final MainActivity mainActivity;

    public MainActivityModule(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

}
