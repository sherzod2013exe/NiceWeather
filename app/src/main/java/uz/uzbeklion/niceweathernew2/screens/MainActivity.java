package uz.uzbeklion.niceweathernew2.screens;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.uzbeklion.niceweathernew2.App;
import uz.uzbeklion.niceweathernew2.BuildConfig;
import uz.uzbeklion.niceweathernew2.R;
import uz.uzbeklion.niceweathernew2.retrofit.ApiInterface;
import uz.uzbeklion.niceweathernew2.adapters.WeatherAdapter;
import uz.uzbeklion.niceweathernew2.models.Example;
import uz.uzbeklion.niceweathernew2.screens.dagger.DaggerMainActivityComponent;
import uz.uzbeklion.niceweathernew2.screens.dagger.MainActivityComponent;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.list)
    RecyclerView recyclerView;

    @Inject
    WeatherAdapter weatherAdapter;

    @Inject
    ApiInterface apiInterface;


    Call<Example> call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        MainActivityComponent component= DaggerMainActivityComponent.builder()
                .weatherComponent(App.get(this).getComponent())
                .build();
        component.injectMainActivity(this);
        call = apiInterface.getData(61,43,"metric", BuildConfig.API_KEY);
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                weatherAdapter.addAfter(response.body().getList());
                recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                recyclerView.setAdapter(weatherAdapter);
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.bind(this).unbind();
        if(call!=null)  call.cancel();
        call = null;
    }
}
