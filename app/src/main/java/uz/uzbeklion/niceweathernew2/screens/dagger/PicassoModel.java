package uz.uzbeklion.niceweathernew2.screens.dagger;

import android.content.Context;

import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import okhttp3.OkHttpClient;

/**
 * Created by Sherzod on 28.03.2018.
 */

public class PicassoModel  {
    Picasso picasso;
    private static PicassoModel picassoModel;
    private PicassoModel(Context context) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .build();
        picasso = new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(okHttpClient))
                .build();
    }

    public Picasso getPicasso() {
        return picasso;
    }

    public static PicassoModel getInstantPicasso(Context context){
        if(picassoModel == null){
            picassoModel = new PicassoModel(context);
        }
            return picassoModel;

    }
}
